from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt
from .forms import ReceiptForm

@login_required
def receipt_list(request):
    user = request.user
    user_receipts = Receipt.objects.filter(purchaser=user)
    return render(request, 'receipt/receipt_list.html', {'receipts': user_receipts})

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user  # Set the current user as the purchaser
            receipt.save()
            return redirect('home')  # Redirect to the 'home' page upon successful creation
    else:
        form = ReceiptForm()
    return render(request, 'receipt/create_receipt.html', {'form': form})
